from hackerrank.funny_string import funnyString
import unittest

class FunnyStringTest(unittest.TestCase) :
    def test_a_b_c_d_is_funny(self) :
        letters = 'abcd'
        is_funny = funnyString(letters)
        self.assertTrue('Funny', is_funny)

    def test_e_f_g_h_is_notfunny(self) :
        letters = 'efgh'
        is_notfunny = funnyString(letters)
        self.assertTrue('Not Funny', is_notfunny)

if __name__ == '__main__' :
    unittest.main()