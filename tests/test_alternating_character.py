from hackerrank.alternating_character import alternatingCharacters
import unittest

class AlternatingCharactersTest(unittest.TestCase) :
    def test_A_A_A_A(self) :
        letters = 'AAAA'
        is_alternating = alternatingCharacters(letters)
        self.assertEqual(3,is_alternating)

    def test_B_B_B_B_B(self) :
        letters = 'BBBBB'
        is_alternating = alternatingCharacters(letters)
        self.assertEqual(4,is_alternating)

    def test_A_B_A_B_A_B_A_B(self) :
        letters = 'ABABABAB'
        is_alternating = alternatingCharacters(letters)
        self.assertEqual(0,is_alternating)

    def test_B_A_B_A_B_A(self) :
        letters = 'BABABA'
        is_alternating = alternatingCharacters(letters)
        self.assertEqual(0,is_alternating)

    def test_A_A_A_B_B_B(self) :
        letters = 'AAABBB'
        is_alternating = alternatingCharacters(letters)
        self.assertEqual(4,is_alternating)

if __name__ == '__main__' :
    unittest.main()
