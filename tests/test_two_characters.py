from hackerrank.two_characters import alternate
import unittest

class TwoCharacters(unittest.TestCase) :
    def test_two_characters1(self) :
        letters = 'beabeefeab'
        is_twocharacters = alternate(letters)
        self.assertEqual(5, is_twocharacters)

    def test_two_characters2(self) :
        letters = 'asdcbsdcagfsdbgdfanfghbsfdab'
        is_twocharacters = alternate(letters)
        self.assertEqual(8, is_twocharacters)
