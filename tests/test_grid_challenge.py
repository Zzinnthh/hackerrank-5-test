from hackerrank.grid_challenge import gridChallenge
import unittest

class GridChallenge(unittest.TestCase) :
    def test_grid1_isyes(self) :
        lst_string = ['abc', 'ade', 'efg']
        is_yes = gridChallenge(lst_string)
        self.assertEqual('Yes', is_yes)

    def test_grid2_isyes(self) :
        lst_string = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        is_yes = gridChallenge(lst_string)
        self.assertEqual('Yes', is_yes)

    def test_grid3_isno(self) :
        lst_string = ['mpxz', 'abcd', 'wlmf']
        is_no = gridChallenge(lst_string)
        self.assertEqual('No', is_no)


if __name__ == '__main__' :
    unittest.main()

