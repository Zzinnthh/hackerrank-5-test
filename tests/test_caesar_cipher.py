from hackerrank.caesar_cipher import caesarCipher
import unittest

class CaesarCipherTest(unittest.TestCase) :
    def test_caesar_give_number_2(self) :
        letters = 'middle-Outz'
        number = 2
        is_caesar =  caesarCipher(letters,number)
        self.assertEqual('okffng-Qwvb',is_caesar)

    def test_caesar_give_number_5(self) :
        letters = 'Always-Look-on-the-Bright-Side-of-Life'
        number = 5
        is_caesar =  caesarCipher(letters,number)
        self.assertEqual('Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj',is_caesar)

if __name__ == '__main__' :
    unittest.main()
